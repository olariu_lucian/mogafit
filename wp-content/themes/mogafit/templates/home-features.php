<section class="section-features section-padding">
	<div class="container">

			<?php if( have_rows('features_magazin','option') ):
				while ( have_rows('features_magazin','option') ) : the_row(); ?>
				<div class="col-md-4 col-sm-4 col-sm-12 bg-img" style="background:url('<?php the_sub_field('imagine_de_fundal'); ?>') no-repeat; background-size:contain; background-position:center center;">
					<div class="row">
					<a href="<?php the_sub_field('link'); ?>">
					<div class="media">
						
					  <div class="media-left media-top">
					    <a href="#">
					     <?php the_sub_field('icoana'); ?>
					    </a>
					  </div>
					  <div class="media-body">
					    <h4 class="media-heading"><?php the_sub_field('titlu'); ?></h4>
					    <p><?php the_sub_field('subtitlu'); ?></p>
					  </div>
				
					</div>
				</a>
				</div>
				</div>
				
		
				<?php
				endwhile;
			else :
				// no rows found
			endif;
			?>
	</div>
</section>