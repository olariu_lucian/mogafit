  <section class="hp-slider">
      <div class="slider-container" style="visibility: hidden">
              <div class="bx-loading"></div>
          <div class="slider">
               <?php
               // check if the repeater field has rows of data
               if( have_rows('home_slider','option') ):
                   while ( have_rows('home_slider','option') ) : the_row();
                      $image = get_sub_field('imagine');    
                      $position = get_sub_field('pozitie_descriere');
                      switch($position){
                        case  "left":
                                $box_position = 'col-md-offset-0';
                                break;
                        case  "middle":
                                $box_position = 'col-md-offset-4';
                                break;
                        case  "right":
                                $box_position = 'col-md-offset-8';
                                break;
                      }
                      ?>
                      <div class="slide-item" style="background-image:url('<?php echo $image['url']; ?>'); ">
                         <div class="container">
                         	
	                         <div class="col-xs-10 col-xs-offset-1 col-md-4 <?php echo $box_position; ?> text-center">
                            <div class="overlay-white-border">
                              <div class="description">
	                             <a href="<?php the_sub_field('url');?>"><h2><?php the_sub_field('titlu'); ?></h2></a>
	                              <div class="hidden-xs hidden-sm"><?php the_sub_field('descriere');?></div>
                              </div>
                               <div class="clearfix"></div>
                            </div>
	                        
	                
	                         </div>
                        </div>
                    </div>
                       <?php
                   endwhile;
               else :
                   // no rows found
               endif;
               ?>
         </div>
        </div>
</section>