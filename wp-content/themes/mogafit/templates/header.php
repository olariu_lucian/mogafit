<?php $logo = get_field('logo','option'); ?>
<header class="banner" xmlns:http="http://www.w3.org/1999/xhtml">
  <div class="topbar">
    <div class="container">
      <div class="topbar-links-left pull-left">
        <?php
        if (has_nav_menu('topleft_navigation')) :
          wp_nav_menu(['theme_location' => 'topleft_navigation', 'menu_class' => 'list-inline list-unstyled']);
        endif;
        ?>
      </div>
      <div class="topbar-links-right pull-right">
        <?php
        if (has_nav_menu('topright_navigation')) :
          wp_nav_menu(['theme_location' => 'topright_navigation', 'menu_class' => 'list-inline list-unstyle pull-right']);
        endif; ?>
      </div>
    </div>
  </div>
<div class="main-header">
  <div class="container">

    <div class="col-xs-12 col-sm-2 col-sm-push-5 col-md-2 col-md-push-5 text-center header-logo">
    <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><?php //bloginfo('name'); ?>
      <img class="img-responsive" src="<?php  echo $logo['url'];//echo get_template_directory_uri();?>" alt="">
    </a>
  </div>

  <div class="header-right col-sm-5 col-sm-push-5 col-md-5 col-md-push-5 no-padding">
        <div class="header_cart headercart-block pull-right">
      <?php
      // Woo commerce Header Cart
      if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) : ?>
        <div class="cart">
          <?php global $woocommerce;
          ob_start();
          //if(!is_cart() && !is_checkout()) {
          ?>
          <div id="shopping_cart" class="shopping_cart dropdown" title="<?php _e('View your shopping cart', 'mogafit'); ?>">
            <a id="dLabel" class="cart-contents" data-target="#" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="<?php _e('View your shopping cart', 'mogafit');?>">
               <i class="fa fa-cart-arrow-down"></i>
              &nbsp;
              <span class="hidden-xs"><?php _e('COȘ ','mogafit');?></span>
              <span class="hidden-xs"><?php echo sprintf(_n('(%d)', '(%d)', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
              <span class="visible-xs-inline-block"><?php echo sprintf(_n('(%d)', '(%d)', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count)?></span>
              <span class="hidden-xs hidden-sm"> <?php if($woocommerce->cart->cart_contents_count > 0 ) { echo $woocommerce->cart->get_cart_total();}?> </span>
              <span class="caret"></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="dLabel">
              <?php global $woocommerce; ?>
              <?php dynamic_sidebar('sidebar-headercart');  ?>
            </div>
          </div>
        </div>
      <?php //}
      endif;
      ?>
    </div> 
    
    <div class="header-search">
      <div class="menu-item menu-search">
        <?php $placeholder = __('Search …','mogafit');
        $title = __('Search for:','mogafit');?>
        <form role="search" method="get" class="search-form" action="<?php echo home_url('/');?>">
         <label>
            <span class="screen-reader-text"><?php _e( 'Search for:', 'label' );?></span>
            <input type="search" class="search-field input-sm" placeholder="<?php echo $placeholder; ?>" value="<?php get_search_query();?>" name="s" title="<?php echo $title;?>" />
          </label>
          <button type="submit" class="header-search-btn" ><i class="fa fa-search"></i></button>
          <input type="hidden" name="post_type[]" value="product" />
          <input type="hidden" name="post_type[]" value="product_variation" />
        </form>
        </div>
    </div>
  </div>

<div class="col-xs-12 col-sm-5 col-sm-pull-7 col-md-5 col-md-pull-7 navbar text-center navbar-default no-padding">
    
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed <?php //if(!is_front_page()){echo 'margin-top';} ?>"
                data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only"><?= __('Toggle navigation', 'mogafit'); ?></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <nav class="collapse navbar-collapse text-center main-nav no-padding" role="navigation">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
      </nav>
 
  </div>

</div>
</div>

</header>

<?php if(!is_front_page()) { ?>
    <div class="page-header-container text-center">
      <div class="clearfix">
        <?php get_template_part('templates/page', 'header'); ?>
        <?php if(!is_front_page() && function_exists('yoast_breadcrumb') ) { ?>
          <div class="container"><div class="row clearfix"><?php yoast_breadcrumb('<p id="breadcrumbs" class="col-xs-12">','</p>'); ?></div></div>
        <?php } ?>
      </div>
      
    </div>
<?php } ?>

