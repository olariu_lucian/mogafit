<footer id="footer" class="content-info ">
  <div class="container">
    <div class="row clearfix">

    <?php dynamic_sidebar('sidebar-footer'); ?>
    </div>
    <div class="footer-seperator"></div>
      <div class="footer-copyright">
       	<?php the_field('text_copyright','option');?>
   	  </div>
  </div>
    <div class="copyright">     
    </div>
</footer>
