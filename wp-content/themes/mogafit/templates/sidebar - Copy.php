<?php
if(is_post_type_archive('agri_products') || is_tax('product-categories') || is_singular('agri-products')) {
	$current_term = get_queried_object();
	print_r($current_term);
	$taxonomyName = "product-categories";
	$side_categories_args = array('taxonomy' => $taxonomyName, 'hide_empty' => false, 'orderby' => 'include', 'order' => 'ASC', 'parent'   => 0);
	$parent_categories = get_terms($side_categories_args ); ?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php foreach ($parent_categories as $side_parent_category) {
//		$object_terms =  wp_get_object_terms( $side_parent_category->term_id, $taxonomyName);
//		var_dump($object_terms);
		$cterms = get_terms(array('taxonomy' => $taxonomyName, 'child_of' => $side_parent_category->term_id, 'parent' => $side_parent_category->term_id, 'orderby' => 'group_name', 'hide_empty' => false));
	?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading-<?php echo $side_parent_category->term_id; ?>">
			<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $side_parent_category->term_id; ?>" aria-expanded="<?php if(  $side_parent_category->term_id == ($current_term->term_id || $current_term->parent )) {echo "true";} else {echo "false";}?>" aria-controls="collapse-<?php echo $side_parent_category->term_id; ?>">+</a>
				<a href="<?php echo get_term_link ($side_parent_category->term_id);?>"><?php echo $side_parent_category->name;?></a></h4>

		</div>
		<div id="collapse-<?php echo $side_parent_category->term_id; ?>" class="panel-collapse collapse <?php if(($current_term->term_id || $current_term->parent ) == $side_parent_category->term_id) {echo "in";} else {echo "";}?>" role="tabpanel" aria-labelledby="heading-<?php echo $side_parent_category->term_id; ?>">
			<div class="panel-body">
				<?php $termchildren = get_term_children( $side_parent_category->term_id, 'product-categories' );
				foreach ($cterms as $cterm) {
					$gcterms = get_terms(array('taxonomy' => $taxonomyName, 'child_of' => $cterm->term_id, 'orderby' => 'group_name', 'hide_empty' => false)); ?>
					<div class="childTerms">
						<div class="" role="tab" id="heading-<?php echo $cterm->term_id; ?>">
							<h5 class='childTermHeading'>
								<?php if($gcterms) { ?>
								<a role="button" data-toggle="collapse" data-parent="#sub-accordion" href="#collapse-<?php echo $cterm->term_id; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $cterm->term_id; ?>">+</a>
								<?php } ?>
								<a href="<?php echo get_term_link ($cterm->term_id);?>"><?php echo htmlspecialchars($cterm->name); ?></a>
							</h5>
						</div>
						<?php if($gcterms) { ?>
							<div class="">
								<div id="collapse-<?php echo $cterm->term_id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $cterm->term_id; ?>"><?php
									foreach ($gcterms as $gcterm) { ?>
										<a href="<?php echo get_term_link ($gcterm->term_id);?>"><?php echo htmlspecialchars($gcterm->name); ?></a>
									<?php } // end grand-child terms ?>
								</div>
							</div>
							<?php }	?>
					</div>

				<?php } // end child
				?>
			</div>
		</div>
	</div>
	<?php } // end parent ?>
</div>
	<?php
	dynamic_sidebar('sidebar-products');
} else {
	dynamic_sidebar('sidebar-primary');
}
 ?>
<?php  ?>
