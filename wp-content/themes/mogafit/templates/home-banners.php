<?php
//echo '<pre>', var_dump(get_field('banner_stânga_sus','option')), '</pre>';
?>
<section class="section-banners">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-md-offset-3 text-center">
        
          <h2 class="section-title"><?php the_field('titlu_banners','option'); ?></h2>
      
      </div>

      <div class="col-md-12">
        <div class="row clearfix">
     
          <div class="col-md-4 col-sm-4 col-xs-12 column-third">
            <div class="row">
              <div class="col-md-12">
                 <div class="banner-content-container">
                  <a href="<?php the_field('banner_stanga_sus_link','option') ?>">
                   <div class="banner-overlay"></div>
                   <div class="line-horizontal"></div>
                   <div class="line-vertical"></div>
                   <div class="banner-hover"></div>
                  </a>
                   <?php
                   $id = get_field('banner_stânga_sus','option');
                   $image = wp_get_attachment_image_src($id, 'full', array('class'=>'img-responsive banner-margin-bottom'));
                   ?>
                   <img class="img-responsive center-block banner-margin-bottom" src="<?php echo $image[0]; ?>" />
                 </div>
              </div>
              <div class="col-md-12">
                <div class="banner-content-container">
                 <a href="<?php the_field('banner_stânga_jos_link','option') ?>">
                  <div class="banner-overlay"></div>
                   <div class="line-horizontal"></div>
                   <div class="line-vertical"></div>
                   <div class="banner-hover"></div>
                 </a>
                  <?php
                  $id = get_field('banner_stanga_jos','option');
                  $image = wp_get_attachment_image_src($id, 'full', array('class'=>'img-responsive banner-margin-bottom'));
                  ?>
                  <img class="img-responsive center-block banner-margin-bottom" src="<?php echo $image[0]; ?>" />
                </div>
              </div>

            </div>
          </div><!-- column third end -->
          <div class="col-md-4 col-sm-4 col-xs-12 column-third">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class=" banner-content-container-middle">
                    <a href="<?php the_field('banner_mijloc_link','option') ?>">
                      <div class="banner-overlay"></div>
                       <div class="line-horizontal"></div>
                       <div class="line-vertical"></div>
                       <div class="banner-hover"></div>
                     </a>
                    <?php
                    $id = get_field('banner_mijloc','option');
                    $image = wp_get_attachment_image_src($id, 'full', array('class'=>'img-responsive banner-margin-bottom'));
                    ?>
                    <img class="img-responsive center-block banner-margin-bottom" src="<?php echo $image[0]; ?>" />
                  </div>
                </div>
              </div>
            </div>
          </div><!-- column third end -->
          <div class="col-md-4 col-sm-4 col-xs-12 column-third">
            <div class="row">
              <div class="col-md-12">
                <div class="banner-content-container">
                    <a href="<?php the_field('banner_dreapta_sus_link','option') ?>">
                      <div class="banner-overlay"></div>
                       <div class="line-horizontal"></div>
                       <div class="line-vertical"></div>
                       <div class="banner-hover"></div>
                     </a>
                  <?php
                  $id = get_field('banner_dreapta_sus','option');
                  $image = wp_get_attachment_image_src($id, 'full', array('class'=>'img-responsive banner-margin-bottom'));
                  ?>
                  <img class="img-responsive center-block banner-margin-bottom" src="<?php echo $image[0]; ?>" />
                </div>
              </div>
              <div class="col-md-12">
                <div class="banner-content-container">
                   <a href="<?php the_field('banner_dreapta_jos_link','option') ?>">
                      <div class="banner-overlay"></div>
                       <div class="line-horizontal"></div>
                       <div class="line-vertical"></div>
                       <div class="banner-hover"></div>
                     </a>
                  <?php
                  $id = get_field('banner_dreapta_jos','option');
                  $image = wp_get_attachment_image_src($id, 'full', array('class'=>'img-responsive banner-margin-bottom'));
                  ?>
                  <img class="img-responsive center-block banner-margin-bottom" src="<?php echo $image[0]; ?>" />
                </div>
              </div>
            </div>
          </div> <!-- column third end -->
      
        </div>
      </div>

    </div>
  </div>
</section>
