<section class="section-traits section-padding section-gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">		      
		          <h2 class="section-title"><?php the_field('sectiune_avantaje_titlu','option'); ?></h2>
		     </div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php			
				if( have_rows('avantaje', 'option') ): ?>

				<ul class="list-traits list-inline list-avantaje text-center"> 	
				  <?php  while ( have_rows('avantaje' , 'option') ) : the_row(); ?>
				  	<li>
				       	<?php the_field('icoana','option'); ?>
				        <span><?php the_sub_field('titlu_avantaj'); ?> </span>
				    </li>
				<?php
				    endwhile;
				else :

				endif;
				?>
		</div>
	</div>
</section>