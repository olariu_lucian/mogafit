<section class="section-clienti section-padding">
	<div class="container margin-bottom">
		<div class="row clearfix">
			<h2 class="sep-simple"><span><?php the_field('sectiune_clienti_titlu','option'); ?></span></h2>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<?php			
				if( have_rows('clienti', 'option') ): ?>

					
				  <?php  while ( have_rows('clienti' , 'option') ) : the_row(); ?>
				  <?php //echo '<pre>',var_dump(the_sub_field('imagine')),'</pre>' ?>
			
					  <div class="col-xs-6 col-sm-4 col-md-2 inline-block clienti-column clearfix">
							<img class="img-responsive" src="<?php the_sub_field('imagine'); ?>" />
					  </div>

				<?php
				    endwhile;
				else :

				endif;
				?>
		</div>
	</div>

</section>