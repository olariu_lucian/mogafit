<section class="section-produse section-padding">
<?php
// $background = get_field('background_products_carousel','option');
// $meta_query   = WC()->query->get_meta_query();
$no_products = get_field('numar_produse_afisate_pe_homepage','option');
$meta_query[] = array(
	'key'   => '_featured',
	'value' => 'yes'
);
$args = array(
	'post_type'   =>  'product',
	'posts_per_page' => $no_products,
	'orderby'     =>  'date',
	'order'       =>  'DESC',
	'meta_query'  =>  'total_sales',
);
$loop = new WP_Query( $args );?>
	
	<div class="container">
		<div class="row clearfix">
			<h2 class="sep-simple"><span><?php the_field('cele_mai_vandute_produse_titlu','option'); ?></span></h2>
		</div>
	</div>

	<div class="container container-border clearfix">
	
			<?php while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
				<?php get_template_part('templates/content', 'product-carousel'); ?>
			<?php endwhile; ?>
		
	</div>
</section>
<?php wp_reset_query(); ?>