<article <?php post_class('col-xs-12 col-sm-6'); ?>>
  <div class="white-box clearfix">
  <?php
  $xclass = "col-xs-12";
  if(has_post_thumbnail()) { ?>
    <div class="col-xs-4 no-padding-left">
      <?php the_post_thumbnail('medium',array('class' => 'img-responsive'));?>
    </div>
    <?php
    $xclass = "col-xs-8";

  } ?>
  <div class="<?php echo $xclass; ?> no-padding">
    <header>
      <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php // get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-summary">
      <?php the_excerpt(); ?>
      <div class="read-more text-right text-uppercase">
        <a href="<?php the_permalink();?>"><?php _e('Read more','vindeciumbrud');?></a>
      </div>
    </div>
  </div>
  </div>
</article>
