<div class="col-xs-12 home-categories">
	<div class="home-cat-title">
		<h2 class="sep-simple"><strong><?php the_field('homepage_categories_title','option');?></strong></h2>
	</div>
	<?php
	$taxonomy     = 'product_cat';
	$orderby      = 'id';
	$show_count   = 0;      // 1 for yes, 0 for no
	$pad_counts   = 0;      // 1 for yes, 0 for no
	$hierarchical = 0;      // 1 for yes, 0 for no
	$title        = '';
	$empty        = 1;

	$args = array(
		'taxonomy'     => $taxonomy,
		'orderby'      => $orderby,
		'show_count'   => $show_count,
		'pad_counts'   => $pad_counts,
		'hierarchical' => $hierarchical,
		'title_li'     => $title,
		'hide_empty'   => $empty
	);
	$all_categories = get_categories( $args );
	foreach ($all_categories as $cat) {
		if($cat->category_parent == 0) {
			//$category_id = $cat->term_id;
			$category_id = 0;
			$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
			$image = wp_get_attachment_image_src( $thumbnail_id, 'categ-medium',false );
			?>
			<div class="col-xs-12 col-sm-4 ">
				<div class="home-cat">
				<a href="<?php echo get_term_link($cat->slug, 'product_cat') ?>">
					<div class="category-image">
						<?php
						if ( $image ) {
							echo '<img src="' . $image[0] . '" alt="" class="thumbnail img-responsive" />';
						}
						?>
					</div>
					<div class="category-title">
						<?php echo $cat->name ?>
					</div>

				</a>
				</div>
			</div>

			<?php
			//echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';


			$args2 = array(
				'taxonomy'     => $taxonomy,
				'child_of'     => 0,
				'parent'       => $category_id,
				'orderby'      => $orderby,
				'show_count'   => $show_count,
				'pad_counts'   => $pad_counts,
				'hierarchical' => $hierarchical,
				'title_li'     => $title,
				'hide_empty'   => $empty
			);
//				$sub_cats = get_categories( $args2 );
//				if($sub_cats) {
//					foreach($sub_cats as $sub_category) {
//						echo  $sub_category->name ;
//					}
//				}
		}
	}
	?>

	<?php //get_template_part('templates/home','slider'); ?>
</div>