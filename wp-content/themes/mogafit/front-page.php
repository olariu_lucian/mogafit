		<?php get_template_part('templates/home', 'slider'); ?>
		<?php do_action('woocommerce_before_shop_loop');?>
		<?php get_template_part('templates/home', 'features'); ?>
		<?php get_template_part('templates/home', 'banners'); ?>
		<?php get_template_part('templates/home', 'traits'); ?>
		<?php get_template_part('templates/home', 'products'); ?>
		<?php get_template_part('templates/home', 'clienti'); ?>
	<div class="container">
		<div class="row clearfix">
			<?php //get_template_part('templates/home', 'categories'); ?>
			<div class="col-xs-12">
				 <?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('templates/content', 'page'); ?>
				<?php endwhile; ?>
			</div>
		</div>
</div>
