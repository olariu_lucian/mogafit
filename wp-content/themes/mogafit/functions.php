<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/assets.php',        // Scripts and stylesheets
    'lib/extras.php',        // Custom functions
    'lib/cleanup.php',       // Custom functions
    'lib/setup.php',         // Theme setup
    'lib/woocommerce-config.php',         // Theme setup
    'lib/admin-tweaks.php',  // Admin interface changes
    //'lib/disable-stuff.php', // Disable unnecessary WP stuff
    'lib/utils.php',         // Theme setup
    'lib/nav.php',           // Navigation tweaks
    'lib/nav-walker.php',    // Bootstrap 3 walker
    //'lib/sidebar-walker.php',    // Bootstrap 3 walker
    'lib/titles.php',        // Page titles
    'lib/wrapper.php',       // Theme wrapper class
    'lib/customizer.php',     // Theme customizer
    'lib/lc-defaults.php'     // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'mogafit'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

////function definition in functions.php of theme
//function twitter_bootstrap_menu(){
//  echo '
//    <ul id="" class="nav pull-right">
//     <li class="dropdown menu-item">
//      <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.strtoupper(ICL_LANGUAGE_CODE).'</a>
//       <ul class="dropdown-menu">';
//
//  $languages = icl_get_languages('skip_missing=0&orderby=code');
//  if(!empty($languages)){
//    foreach($languages as $l){
//      echo ($l['active'] == 0 ? "<li><a href='".$l['url']."'>" . strtoupper($l['language_code']) ."</a></li>" : NULL);
//    }
//  }
//  echo '
//       </ul>
//     </li>
//    </ul>
//    ';
//}
