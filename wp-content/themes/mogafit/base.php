<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'mogafit'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    $container_class = "container";
    $content_class = "row";

    if(is_front_page()) {
      $container_class = "";
      $content_class = "";
    }
    ?>
    <div class="wrap clearfix <?php echo $container_class; ?>" role="document">
      <div class="content <?php echo $content_class; ?>">
        <main class="main clearfix <?php if (Setup\display_sidebar()) {echo "";} ?>">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside id="sidebar" class="sidebar <?php if(is_post_type_archive('agri_products') || is_tax('product-categories') || is_singular('agri_products')) {echo "sidebar-products";}?> clearfix">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
