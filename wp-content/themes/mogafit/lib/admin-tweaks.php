<?php
namespace Roots\Sage\Extras;
use Roots\Sage\Setup;
/**
 * WP-Admin Tweaks
 */


/**
 * Base for CPT
 */
add_filter('sage/wrap_base', __NAMESPACE__ . '\\sage_wrap_base_cpts'); // Add our function to the sage_wrap_base filter

function sage_wrap_base_cpts($templates) {
    $cpt = get_post_type(); // Get the current post type
    if ($cpt) {
        array_unshift($templates, 'base-' . $cpt . '.php'); // Shift the template to the front of the array
    }
    return $templates; // Return our modified array with base-$cpt.php at the front of the queue
}


/**
 * ADD Featured Image column and thumbnails
 */
// Add the posts and pages columns filter. They can both use the same function.
add_filter('manage_posts_columns',  __NAMESPACE__ . '\\sage_add_post_thumbnail_column', 2);
add_filter('manage_pages_columns',  __NAMESPACE__ . '\\sage_add_post_thumbnail_column', 2);

// Add the column
function sage_add_post_thumbnail_column($cols){
    $cols['sage_post_thumb'] = __('Featured Image','mogafit');
    return $cols;
}

// Hook into the posts an pages column managing. Sharing function callback again.
add_action('manage_posts_custom_column', __NAMESPACE__ . '\\sage_display_post_thumbnail_column', 2, 2);
add_action('manage_pages_custom_column', __NAMESPACE__ . '\\sage_display_post_thumbnail_column', 2, 2);

// Grab featured-thumbnail size post thumbnail and display it.
function sage_display_post_thumbnail_column($col, $id){
    switch($col){
        case 'sage_post_thumb':
            if( function_exists('the_post_thumbnail') )
                echo the_post_thumbnail(array(120,70) );
            else
                echo 'Not supported in theme';
            break;
    }
}

/**
 * Login logo url change
 */
function change_wp_login_url() {
    return home_url();
}
add_filter('login_headerurl', __NAMESPACE__ . '\\change_wp_login_url');

/**
 * Login logo title change
 */

function change_wp_login_title() {
    return get_option('blogname');
}
add_filter('login_headertitle', __NAMESPACE__ . '\\change_wp_login_title');

function remove_footer_admin () {
    echo 'Created by <a href="http://www.impactpro.ro" target="_blank">Impact Production.</a>';
}
//add_filter('admin_footer_text', __NAMESPACE__ . '\\remove_footer_admin');

// Hide wp icon and links from the admin bar
function annointed_admin_bar_remove() {
    global $wp_admin_bar;
    /* Remove their stuff */
    $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', __NAMESPACE__ . '\\annointed_admin_bar_remove', 0);

/**
 * Login logo
 */
function my_login_logo() {
    //$admin_logo_field = get_field('logo','option');
    //$upload_dir = wp_upload_dir();
    //$admin_logo = $admin_logo_field['large'];
    $logo = get_field('logo','option');
    ?>
    <style type="text/css">
        body.login div#login h1 a {background-image: url( '<?php echo $logo['url'];?>'); background-size: contain; padding-bottom: 0; width:276px; height:97px; }
        body.login {background-color: #F0F0F0}
        #login {padding-top:30px !important;}
        .g-recaptcha {margin-bottom: 10px !important;}

    </style>
<?php }
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo' );

/**
 * Gravity Forms anchor - disable auto scrolling of forms
 */

add_filter('gform_confirmation_anchor', '__return_false');