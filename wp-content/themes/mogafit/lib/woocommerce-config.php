<?php 
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'vinonesto_dequeue_styles' );
function vinonesto_dequeue_styles( $enqueue_styles ) {
    unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
    unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
    unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
    return $enqueue_styles;
}

/**
 * Display Category images of woocommerce
 */

add_action( 'woocommerce_archive_description', 'woocommerce_category_image', 2 );
function woocommerce_category_image() {
    if ( is_product_category()  ){
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        $cat_term = get_term($cat->term_id);
        $description = $cat_term->description;
        if ( $image && !empty($description) ) {
            echo '<div class="col-xs-2 no-padding-left">';
            echo '<img src="' . $image . '" alt="" />';
            echo '</div>';
        }
    }
}



// Change woocommerce on sale to show percent.

// add_filter( 'woocommerce_sale_flash', 'woocommerce_custom_sales_price', 10, 2 );
// function woocommerce_custom_sales_price( $price, $product ) {
 
//     $percentage = round( $product->regular_price - $product->sale_price);
 
//     return $price . sprintf( __(' <br /><span class="price_save">SAVE &pound;%s', 'woocommerce' ), $percentage . '</span>' );
 
// }
 
add_filter( 'woocommerce_sale_flash', 'woocommerce_custom_variable_sales_price', 10, 2 );
function woocommerce_custom_variable_sales_price() {

    global $product;
 
    if ($product->product_type == 'simple' && $product->is_on_sale() ) {

        $percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );

    }

    elseif ($product->product_type == 'variable' && $product->is_on_sale()) {
         foreach ($product as $prod){
            
            $percentage = round((($product->get_variation_price('max', true) -  $product->get_variation_price('min', true)) / $product->get_variation_price('max', true)) * 100);
        
        }
    }
       
    return '<span class="onsale">'. sprintf( __('-%s', 'woocommerce' ), $percentage . '%' ).'</span>';
 
}

// Change woocommerce price
// Adds "de la" after each product price throughout the shop
// function sv_change_product_price_display( $price ) { 
//     $string = '<span class="de-la">de la </span> ';
//     return $string.$price;
// }
// add_filter( 'woocommerce_get_price_html', 'sv_change_product_price_display' );
// add_filter( 'woocommerce_cart_item_price', 'sv_change_product_price_display' );


//Hide lowest price on product variation
// add_filter( 'woocommerce_variable_sale_price_html', 'hide_variable_max_price', PHP_INT_MAX, 2 );
// add_filter( 'woocommerce_variable_price_html',      'hide_variable_max_price', PHP_INT_MAX, 2 );
// function hide_variable_max_price( $price, $_product ) {
//     $min_price_regular = $_product->get_variation_regular_price( 'min', true );
//     $min_price_sale    = $_product->get_variation_sale_price( 'min', true );
//     return ( $min_price_sale == $min_price_regular ) ?
//         wc_price( $min_price_regular ) :
//         '<del>' . wc_price( $min_price_regular ) . '</del>' . '<ins>' . wc_price( $min_price_sale ) . '</ins>';
//}

add_filter( 'woocommerce_variable_sale_price_html', 'bbloomer_variation_price_format', 10, 2 );
 
add_filter( 'woocommerce_variable_price_html', 'bbloomer_variation_price_format', 10, 2 );
 
function bbloomer_variation_price_format( $price, $product ) {
 
    // Main Price
    $prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
    $price = $prices[0] !== $prices[1] ? sprintf( __( '<span class="hidden-xs">De la: </span> %1$s <span class="hidden-xs">(TVA inclus)</span>', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
     
    // Sale Price
    $prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
    sort( $prices );
    $saleprice = $prices[0] !== $prices[1] ? sprintf( __( 'De la: %1$s (TVA inclus)', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
     
    if ( $price !== $saleprice ) {
    $price = '<del>' . $saleprice . '</del> <ins>' . $price . '</ins></span>';
    }
    return $price;

}



/**
 * remove coupon forms if you don't want a coupon for a free cart
 */
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

/**
 * remove product title
 */
remove_action('woocommerce_single_product_summary','woocommerce_template_single_title',5);


// add additional info tab on the left product details
add_action('woocommerce_single_product_summary', 'woocommerce_product_additional_information_tab',21);

/**
 * Move product box price after image
 */
remove_action( 'woocommerce_after_shop_loop_item_title','woocommerce_template_loop_price',10 );
add_action( 'woocommerce_before_shop_loop_item_title','woocommerce_template_loop_price',10 );

// Remove tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {

    //unset( $tabs['description'] );      	// Remove the description tab
    //unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}



// Hide count
add_filter( 'woocommerce_subcategory_count_html', 'vindeciumbrud_hide_category_count' );
function vindeciumbrud_hide_category_count() {
    // No count
}

/**
 * Move price before add to cart
 */

remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10);
add_action('woocommerce_single_product_summary','woocommerce_template_single_price',22);

/**
 * Move Product Meta (category) below additional information
 */

remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
add_action('woocommerce_single_product_summary','woocommerce_template_single_meta',21);


/**
 * Woocommerce Social Icons
 */
add_action('woocommerce_single_product_summary','woo_social_sharing_buttons', 50);
function woo_social_sharing_buttons() {
    // Show this on post and page only. Add filter is_home() for home page
    //global $post;
    // Get current page URL
    $shortURL = get_permalink();

    // Get current page title
    $shortTitle = get_the_title();

    // Construct sharing URL without using any script
    $twitterURL = 'https://twitter.com/intent/tweet?text='.$shortTitle.'&amp;url='.$shortURL.'&amp;via=Vinonesto';
    $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$shortURL;
    $googleURL = 'https://plus.google.com/share?url='.$shortURL;
    //$bufferURL = 'https://bufferapp.com/add?url='.$shortURL.'&amp;text='.$shortTitle;
    $pinterestURL = 'http://pinterest.com/pin/create/button/?url='.$shortURL.'&description='.$shortTitle;
    $stumbleURL = 'http://www.stumbleupon.com/submit?url='.$shortURL.'&title='.$shortTitle;

    // Add sharing button at the end of page/page content
    $content = '';
    $content .= '<div class="woo-social text-center hidden-print">';
    $content .= '<a class="woo-share-link btn btn-sm woo-twitter" href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter"></i> <span class="hidden-xs">Twitter</span></a>';
    $content .= '<a class="woo-share-link btn btn-sm woo-facebook" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook"></i> <span class="hidden-xs">Facebook</span></a>';
    $content .= '<a class="woo-share-link btn btn-sm woo-googleplus" href="'.$googleURL.'" target="_blank"><i class="fa fa-google-plus"></i> <span class="hidden-xs">Google+</span></a>';
    $content .= '<a class="woo-share-link btn btn-sm woo-pinterest" href="'.$pinterestURL.'" target="_blank"><i class="fa fa-pinterest"></i> <span class="hidden-xs">Pinterest</span></a>';
    $content .= '<a class="woo-share-link btn btn-sm woo-stumble" href="'.$stumbleURL.'" target="_blank"><i class="fa fa-stumbleupon"></i> <span class="hidden-xs">StumbleUpon</span></a>';
    //$content .= '<a class="woo-share-link woo-buffer" href="'.$bufferURL.'" target="_blank">Buffer</a>';
    $content .= '</div>';
    echo $content;
};

/**
 * Add cart in topbar right
 */

//add_filter('wp_nav_menu_items',__NAMESPACE__ . '\\add_cart_to_menu', 10, 2); NOT WORKING

function add_cart_to_menu ( $items, $args ) {
    if ( $args->theme_location == 'topright_navigation') {
        global $woocommerce;
        $items .= '<li class="menu-item menu-cart">';
        $items .= ' <div class="header_cart headercart-block">';
          if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) :
              $items .= '<div class="cart togg">
              <?php global $woocommerce;
              ob_start();?>
              <div id="shopping_cart" class="shopping_cart tog" title="'. __('View your shopping cart', 'woothemes').'">
                <a class="cart-contents" href="'. esc_url($woocommerce->cart->get_cart_url()) .'" title="'. __('View your shopping cart', 'woothemes').'"><i class="fa fa-shopping-cart"></i>
                '. __('Cart: ','woothemes') .'
                '. sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count).'&nbsp;
                '. $woocommerce->cart->get_cart_total() .'</a>
              </div>
              <?php global $woocommerce; ?>
              <?php //templatemela_get_widget(\'header-widget\'); ?>
            </div>';
           endif;
        $items .= '</div>';
        $items .= '</li>';
    }
    return $items;
}

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    ob_start();
    //if(!is_cart() && !is_checkout()) {
    ?>
    <a  id="dLabel" class="cart-contents" data-target="#" href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="<?php _e('View your shopping cart', 'mogafit');?>">
        <i class="fa fa-cart-arrow-down"></i>
        &nbsp;
        <span class="hidden-xs"><?php _e('COȘ ','mogafit');?></span>
        <span class="hidden-xs"><?php echo sprintf(_n('(%d)', '(%d)', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span>
        <span class="visible-xs-inline-block"><?php echo sprintf(_n('(%d)', '(%d)', $woocommerce->cart->cart_contents_count,
                'woothemes'), $woocommerce->cart->cart_contents_count)?></span>
        <span class="hidden-xs hidden-sm"> <?php if($woocommerce->cart->cart_contents_count > 0 ) { echo $woocommerce->cart->get_cart_total();}?>  </span>
        <span class="caret"></span>
    </a>

    <?php
    //}
    $fragments['a.cart-contents'] = ob_get_clean();
    return $fragments;
}

/**
* Replace the placeholder image.
**/
add_action( 'init', 'custom_fix_thumbnail' );

function custom_fix_thumbnail() {
    add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');

    function custom_woocommerce_placeholder_img_src( $src ) {
        $upload_dir = wp_upload_dir();
        $uploads = untrailingslashit( $upload_dir['baseurl'] );
        $src = $uploads . '/2016/07/placeholder-product-300x300.png';

        return $src;
    }
}

/**
 * Add Boostrap style to woocommerce forms
 */

add_filter('woocommerce_form_field_args',  'wc_form_field_args',10,3);
function wc_form_field_args($args, $key, $value) {
    //$args['class'] = array( 'form-group' );
    $args['input_class'] = array( 'form-control input-md' );
    return $args;
}

/**
 * hide country field in checkout
 */
function custom_override_checkout_fields( $fields )
{
    unset($fields['billing']['billing_country']);
    return $fields;
}
add_filter('woocommerce_checkout_fields','custom_override_checkout_fields');


// Remove product description title 
add_filter( 'woocommerce_product_description_heading', 'remove_product_description_heading' );
function remove_product_description_heading() {
return '';
}

// Display only 3 similar products

function woo_related_products_limit() {
  global $product;
    
    $args['posts_per_page'] = 6;
    return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 3; // 4 related products
    $args['columns'] = 3; // arranged in 2 columns
    return $args;
}

// Remove product meta from single product

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

// Fix not to add another item to cart on page refresh

/**
 * Set a custom add to cart URL to redirect to
 * @return string
 */

// function custom_add_to_cart_redirect() {
    
//     //return home_url();
// }
// add_filter( 'woocommerce_add_to_cart_redirect', 'custom_add_to_cart_redirect' );

// Remove product meta filter
add_filter( 'wc_product_sku_enabled', '__return_false' );