<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  //return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'mogafit') . '</a>';
  return '&hellip;';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


/**
*  Change the Options Page capability to 'manage_options'
*/

if( function_exists('acf_set_options_page_capability') ) {
  acf_set_options_page_capability( 'manage_options' );
}
/**
 * Hide ACF field group menu item
 */
//add_filter('acf/settings/show_admin', '__return_false');

/**
 * Declare Theme Options
 */
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
      'page_title' 	=> 'Theme General Settings',
      'menu_title'	=> 'Theme Options',
      'parent_slug' => 'themes.php',
      'menu_slug' 	=> 'theme-general-settings',
      'capability'	=> 'edit_posts',
      'redirect'		=> false
  ));
}

/**
 * Social Share Icons
 */
add_action('social_share_icons', __NAMESPACE__ . '\\salutarmente_social_sharing_buttons', 10);
function salutarmente_social_sharing_buttons() {
  // Show this on post and page only. Add filter is_home() for home page
  global $post;
  // Get current page URL
  $shortURL = get_permalink();

  // Get current page title
  $theTitle = get_the_title();
  $shortTitle = str_replace(' ','%20',$theTitle);

  // Construct sharing URL without using any script
  $twitterURL = 'https://twitter.com/intent/tweet?text='.$shortTitle.'&amp;url='.$shortURL.'&amp;via=sallutarmente';
  $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$shortURL;
  $googleURL = 'https://plus.google.com/share?url='.$shortURL;
  //$bufferURL = 'https://bufferapp.com/add?url='.$shortURL.'&amp;text='.$shortTitle;
  $pinterestURL = 'http://pinterest.com/pin/create/button/?url='.$shortURL.'&amp;description='.$shortTitle;
  $stumbleURL = 'http://www.stumbleupon.com/submit?url='.$shortURL.'&amp;title='.$shortTitle;

  // Add sharing button at the end of page/page content
  $content = '';
  $content .= '<i class="icon-bullhorn"></i> Condividere su: ';
  $content .= '<div class="social-share-icons text-center hidden-print">';
  $content .= '<a class="gb-share-link btn btn-sm woo-facebook" href="'.$facebookURL.'" target="_blank"><i class="icon-facebook"></i> <span class="hidden-xs">Facebook</span></a>';
  $content .= '<a class="gb-share-link btn btn-sm woo-twitter" href="'. $twitterURL .'" target="_blank"><i class="icon-twitter"></i> <span class="hidden-xs">Twitter</span></a>';
  $content .= '<a class="gb-share-link btn btn-sm woo-googleplus" href="'.$googleURL.'" target="_blank"><i class="icon-google-plus"></i> <span class="hidden-xs">Google+</span></a>';
  $content .= '<a class="gb-share-link btn btn-sm woo-pinterest" href="'.$pinterestURL.'" target="_blank"><i class="icon-pinterest-p"></i> <span class="hidden-xs">Pinterest</span></a>';
  $content .= '<a class="gb-share-link btn btn-sm woo-stumble" href="'.$stumbleURL.'" target="_blank"><i class="icon-stumbleupon"></i> <span class="hidden-xs">StumbleUpon</span></a>';
  //$content .= '<a class="woo-share-link woo-buffer" href="'.$bufferURL.'" target="_blank">Buffer</a>';
  $content .= '</div>';
  echo $content;
};

/* Back To Top */

add_action( 'wp_footer', __NAMESPACE__ . '\\back_to_top' );
function back_to_top() {
  echo '<a id="totop" href="#" class="text-center" title="Torna Su"><i class="fa fa-angle-up"></i></a>';
}

/**
 * Search Box in menu
 */

//add_filter('wp_nav_menu_items',__NAMESPACE__ . '\\add_search_box_to_menu', 10, 2);

function add_search_box_to_menu ( $items, $args ) {
  if ( $args->theme_location == 'primary_navigation') {
	  $placeholder = __('Search …','mogafit');
	  $title = __('Search for:','mogafit');
    $items .= '<li class="menu-item menu-search">';
    $items .= '<form role="search" method="get" class="search-form" action="'.home_url('/').'">';
    $items .= '<label>
        <span class="screen-reader-text">'. __( 'Search for:', 'label' ).'</span>
        <input type="search" class="search-field input-sm" placeholder="'. $placeholder .'" value="'. get_search_query() .'" name="s" title="'. $title .'" />
    </label>    
    <button type="submit" class="header-search-btn" ><i class="fa fa-search"></i></button>
    <input type="hidden" name="post_type[]" value="product" />
    <input type="hidden" name="post_type[]" value="product_variation" />
</form>';
    $items .= '</li>';
  }
  return $items;
}

/**
 * Language Switcher in menu
 */

add_filter('wp_nav_menu_items', __NAMESPACE__ . '\\new_nav_menu_items', 10, 2);
function new_nav_menu_items($items, $args) {
  // uncomment this to find your theme's menu location
  //echo "args:<pre>"; print_r($args); echo "</pre>";
  // get languages
  $languages = apply_filters( 'wpml_active_languages', NULL, 'skip_missing=0' );
  // add $args->theme_location == 'primary-menu' in the conditional if we want to specify the menu location.
  if ( $languages && $args->theme_location == 'topright_navigation') {
    if(!empty($languages)){
//      echo '<ul id="" class="nav navbar-nav pull-right">';
//      echo '<li class="menu-item lang-switcher">';
//      echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.strtoupper(ICL_LANGUAGE_CODE).'<span class="caret"></span></a>';
//      echo '<ul class="dropdown-menu">';
      foreach($languages as $l){

        //echo ($l['active'] == 0 ? "<li><a href='".$l['url']."'>". strtoupper($l['language_code']) ."</a></li>" : NULL);
        if(!$l['active']){
          // flag with native name
          $items = $items . '<li class="menu-item lang-switch"><a href="' . $l['url'] . '"><img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" /></a></li>';
          //only flag
          //$items = $items . '<li class="menu-item menu-item-language"><a href="' . $l['url'] . '"><img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" /></a></li>';
        }
      }
      //echo '</ul></li></ul>';
    }
  }
  return $items;
}