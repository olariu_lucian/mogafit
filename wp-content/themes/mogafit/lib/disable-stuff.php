<?php

namespace Roots\Sage\DisableTrackbacks;

    /**
     * Disables trackbacks/pingbacks
     *
     * You can enable/disable this feature in functions.php (or lib/setup.php if you're using Sage):
     */

/**
 * Disable pingback XMLRPC method
 */
function filter_xmlrpc_method($methods) {
    unset($methods['pingback.ping']);
    return $methods;
}
add_filter('xmlrpc_methods', __NAMESPACE__ . '\\filter_xmlrpc_method', 10, 1);

/**
 * Remove pingback header
 */
function filter_headers($headers) {
    if (isset($headers['X-Pingback'])) {
        unset($headers['X-Pingback']);
    }
    return $headers;
}
add_filter('wp_headers', __NAMESPACE__ . '\\filter_headers', 10, 1);

/**
 * Kill trackback rewrite rule
 */
function filter_rewrites($rules) {
    foreach ($rules as $rule => $rewrite) {
        if (preg_match('/trackback\/\?\$$/i', $rule)) {
            unset($rules[$rule]);
        }
    }
    return $rules;
}
add_filter('rewrite_rules_array', __NAMESPACE__ . '\\filter_rewrites');

/**
 * Kill bloginfo('pingback_url')
 */
function kill_pingback_url($output, $show) {
    if ($show === 'pingback_url') {
        $output = '';
    }
    return $output;
}
add_filter('bloginfo_url', __NAMESPACE__ . '\\kill_pingback_url', 10, 2);

/**
 * Disable XMLRPC call
 */
function kill_xmlrpc($action) {
    if ($action === 'pingback.ping') {
        wp_die('Pingbacks are not supported', 'Not Allowed!', ['response' => 403]);
    }
}
add_action('xmlrpc_call', __NAMESPACE__ . '\\kill_xmlrpc');


/**
 * Remove version query string from all styles and scripts
 *
 * You can enable/disable this feature in functions.php (or lib/setup.php if you're using Sage):
 * add_theme_support('soil-disable-asset-versioning');
 */
function remove_script_version($src) {
    return $src ? esc_url(remove_query_arg('ver', $src)) : false;
}
add_filter('script_loader_src', __NAMESPACE__ . '\\remove_script_version', 15, 1);
add_filter('style_loader_src', __NAMESPACE__ . '\\remove_script_version', 15, 1);

/**
 * Remove emoji
 */

function disable_wp_emojicons() {

    // all actions related to emojis
    remove_action( 'admin_print_styles', __NAMESPACE__ . '\\print_emoji_styles' );
    remove_action( 'wp_head', __NAMESPACE__ . '\\print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', __NAMESPACE__ . '\\print_emoji_detection_script' );
    remove_action( 'wp_print_styles', __NAMESPACE__ . '\\print_emoji_styles' );
    remove_filter( 'wp_mail', __NAMESPACE__ . '\\wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', __NAMESPACE__ . '\\wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', __NAMESPACE__ . '\\wp_staticize_emoji' );

    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', __NAMESPACE__ . '\\disable_emojicons_tinymce' );
}
add_action( 'init', __NAMESPACE__ . '\\disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}