<?php
// Extended Walker for Bootstrap
class Bootstrap_Walker_Nav_Sidebar extends Walker_Nav_Menu {
	
	private $firstLvlElement = array();

	//start of the sub menu wrap
	function start_lvl(&$output, $depth = 0, $itemId = array()) {
		
		//if more levels are added change the condition for $depth to nr of levels - 1
		$output .= '<div id="collapse'.($depth<=2?$itemId['itemId']:$itemId['childId']).'" class="panel-collapse collapse">
						<div class="panel-body">							
								<ul class="list">';
	}
 
	//end of the sub menu wrap
	function end_lvl(&$output, $depth = 0,$args=array()) {
		$output .= '</ul>
			</div>
		</div>';
	}
 
	//add the description to the menu item output
	function start_el(&$output, $item, $depth = 0, $itemId=array() , $args = array()) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
		$class_names = $value = '';
 
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
 
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . ' panel panel-default"';
 
		$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
 
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
 
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		//if(strlen($item->description)>2){ $item_output .= '<br /><span class="sub">' . $item->description . '</span>'; }
		$item_output .= '</a>';
		$argsAfter = $args->after;
		$argsAfter=str_replace("{%id%}", $itemId['itemId'], $argsAfter);
		
		$item_output .= $argsAfter;
 
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args = array(), &$output ) {
		if ( !$element )
			return;
		if($depth == 0) {
			$this->firstLvlElement = $element;			
		}

		if($depth == 0) {

			$elementId = $this->firstLvlElement->ID;		
		} else {
			$elementId = $element->ID;
		}
		
		$id_field = $this->db_fields['id'];

		//display this element
		if ( isset( $args[0] ) && is_array( $args[0] ) )
			$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
		
		$cb_args = array_merge( array(&$output, $element, $depth, array("itemId"=>$elementId)), $args);	
		call_user_func_array(array($this, 'start_el'), $cb_args);

		$id = $element->$id_field;

		// descend only when the depth is right and there are childrens for this element
		if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

			foreach( $children_elements[ $id ] as $child ){

				if ( !isset($newlevel) ) {
					$newlevel = true;
					//start the child delimiter
					$args['itemId'] = $element->ID; //added this	
					$cb_args = array_merge( array(&$output, $depth, array("itemId"=>$element->ID, 'childId'=>$child->ID)), $args);
					
					call_user_func_array(array($this, 'start_lvl'), $cb_args);
				}

				$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
			}
			unset( $children_elements[ $id ] );
		}

		if ( isset($newlevel) && $newlevel ){
			//end the child delimiter
			$cb_args = array_merge( array(&$output, $depth), $args);
			call_user_func_array(array($this, 'end_lvl'), $cb_args);
		}

		//end this element
		$cb_args = array_merge( array(&$output, $element, $depth), $args);
		call_user_func_array(array($this, 'end_el'), $cb_args);
	}
} // END WALKER