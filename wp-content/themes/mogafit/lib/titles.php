<?php

namespace Roots\Sage\Titles;

/**
 * Page titles
 */
function title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_the_title(get_option('page_for_posts', true));
    } else {
      return __('Latest Posts', 'mogafit');
    }
  } elseif (is_category()) {
    $title = sprintf(__('%s'), single_cat_title('', false));
    return $title;
  }elseif (is_tax()) {
    $term = get_queried_object()->name;
    return $term;
  } elseif (is_archive()) {
    $title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
    return $title;
    //return get_the_archive_title();
    //return get_the_archive_title();
  } elseif (is_search()) {
    return sprintf(__('Search Results for %s', 'mogafit'), get_search_query());
  } elseif (is_404()) {
    return __('Not Found', 'mogafit');
  } else {
    return get_the_title();
  }
}
