/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        baguetteBox.run('.main');
        $('iframe[src*="google.com"]').wrap('<div class="map-container" />');
        $('iframe[src*="youtube.com"]').wrap('<div class="video-container" />');
        $('.entry-content table').wrap('<div class="table-responsive" />').addClass('table table-bordered');
        $('.entry-content table tr:first').addClass('warning text-bold');
        // Scroll to top
        $(window).scroll(function () {
          if ($(this).scrollTop() > 500) {
            $("#totop").fadeIn();
          } else {
            $("#totop").fadeOut();
          }
        });

        $("#totop").click(function () {
          $("body,html").animate({ scrollTop: 0 }, 800 );
          return false;
        });
        // END scroll to top

        //Bootstrap dropdown open on hover
        jQuery(document).ready(function($) {
          var width = $(window).width();
          if (width > 762) {
          $('ul.nav li.dropdown, ul.nav li.dropdown-submenu').hover(function() {
            $(this).find(' > .dropdown-menu').stop(true, true).delay(100).fadeIn();
          }, function() {
            $(this).find(' > .dropdown-menu').stop(true, true).delay(100).fadeOut();
          });
          $('ul.nav li.dropdown, ul.nav li.dropdown-submenu').click(function(){
             var link = $('li.dropdown > a').attr("href");
             location.href = link;
          });
          }
        });
       
        //END Bootstrap dropdown open on hover


        // same height titles
        function SameHeightTitles() {
          $(".wrap").each(function(){
            var largest = 0;
            $(this).find(".product-box h3").each(function(){
              var findHeight = $(this).height();
              if(findHeight > largest){
                largest = findHeight;
              }
            });
            $(this).find(".product-box h3").css({"height":largest+"px","line-height":largest+"px"});
          });

        }

        // end same height titles


        function FooterBottom() {
          var docHeight = $(window).height();
          var footerHeight = $('.content-info').outerHeight();
          var footerTop = $('.content-info').position().top + footerHeight;

          if (footerTop < docHeight) {
            $('.content-info').css('margin-top', (docHeight - footerTop) +0 + 'px');
          }
        }
        $(window).resize(function() {
          new FooterBottom();
        });
        new FooterBottom();
        //new SameHeightTitles();

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        $(document).ready(function(){
          //new SameHeightTitles();
          $('.product-carousel').bxSlider({
            minSlides: 1,
            maxSlides: 4,
            pager:false,
            slideWidth: 214,
            slideMargin: 30,
            moveSlides: 1,
            nextText:'<i class="fa fa-caret-right fa-3x"></i>',
            prevText: '<i class="fa fa-caret-left fa-3x"></i>'
          });


        $('.slider').bxSlider({
          mode:'fade',
          //pager: false,
          auto: true,
          autoHover: true,
          adaptiveHeight: true,
          nextText:'<i class="fa fa-angle-right fa-4x"></i>',
          prevText: '<i class="fa fa-angle-left fa-4x"></i>',
          onSliderLoad: function(){
            $(".hp-slider .slider-container").css("visibility", "visible");
            $('.bx-loading').remove();
          }
          // onSlideBefore:function(){
          //   $('.slider .description').css('display', 'none');
          // },
          // onSlideAfter:function(){
          //   $('.slider .description').fadeIn();
          // }
        });
        });
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
