<?php //get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'mogafit'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>
<div class="woocommerce">
  <ul class="products">
<?php
query_posts($query_string . "&posts_per_page=-1&paged=0");
while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'search'); ?>
<?php endwhile; ?>
  </ul>
</div>
<?php the_posts_navigation(); ?>
