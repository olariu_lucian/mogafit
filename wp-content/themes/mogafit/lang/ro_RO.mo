��          �   %   �      0  
   1     <     ?     U     \     q     �     �     �     �  	   �     �     �  	   �     �       
     ;   *     f     �     �     �  �   �  �  G  	   �     �     �     �       *        J     _     f     x     �     �     �     �      �     �  
     J     ,   Z     �     �     �  �   �                      
              	                                                                                           %d product %s &larr; Older comments Cart:  Comments are closed. Error locating %s for inclusion Featured Image Header Cart Latest Posts Newer comments &rarr; Not Found Pages: Primary Navigation Read more Search Results for %s Search for: Search … Sorry, but the page you were trying to view does not exist. Sorry, no results were found. Topbar Left Topbar Right View your shopping cart You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience. Project-Id-Version: 
POT-Creation-Date: 2016-07-06 11:07+0300
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
X-Poedit-KeywordsList: _e;__;_x;_n
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: assets
X-Poedit-SearchPathExcluded-1: bower_components
X-Poedit-SearchPathExcluded-2: dist
X-Poedit-SearchPathExcluded-3: node_modules
X-Poedit-SearchPathExcluded-4: woocommerce
 %d produs %s &larr; Comentarii mai vechi Coș:  Comentariile sunt închise Eroare în localizarea %s pentru includere Imagine recomandată Coș:  Ultimele articole Comentarii mai noi &rarr; Nu a fost găsit Pagini: Navigare principală Citește mai departe Rezultatele căutării pentru %s Caută după: Caută ... Ne pare rau, dar pagina pe care aţi încercat să o accesați nu există. Ne pare rău, nu s-a gasit nici un rezultat. Topbar stânga Topbar dreapta Vezi coșul de produse Folosesti un browser <strong>vechi</strong>. Te rog <a href=\”http://browsehappy.com/\”>actualizează-ti browserul</a> pentru o experienta mai bună. 