<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mogafit');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3;4WfZ;9*07YlU0Mq?9-@cy<iRbxE,@(N^k).z7Y}UwlQ|puv|7`G8W)n}IP:?Q{');
define('SECURE_AUTH_KEY',  'i6,&*O;?%mib`lY+m74{W>T=e{dZMwF^h?3.falB1x@KU|Xc&M@@oQer/G>mm!jO');
define('LOGGED_IN_KEY',    '^z{>P(A)9F:3N ~JY..X@{Y]ch0n[r-@aMHC F747T#@un0v1Ym)9YGss+rnzV{>');
define('NONCE_KEY',        '{^0`Zv>0X]hOZ*@1ZOieA=VPP3_$; KX&Ff!=Gz~5L7m*F6#LO#n[u) 5H+&^0[_');
define('AUTH_SALT',        'nZh]OlQ;C;3l&F<h;Bv(:]`}VEgk}Q Ns0PD{1>me?Fk7Y_9D9-MIf!Rk)IE#%$i');
define('SECURE_AUTH_SALT', 'QaYm3asUv>ec]L1a`>LS+UKy-A|3@|qP%T^?78*37fvMDq-VfQ@a$/DFFl<%|q]`');
define('LOGGED_IN_SALT',   'ZY@E-M_I:};-(qG,ZnAYH~Jh>BP*,}]U^6;vJ7*;5s L=S}__) bHP^q]vlo?[Wy');
define('NONCE_SALT',       'SVkhxCmFJ48I,#,KU8u_ws 4p>tP_fJd`7`TKFA9N,etM&PtS< ]LM>=p.vk3a0,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mgf16_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
